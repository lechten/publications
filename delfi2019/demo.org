# Local IspellDict: en
#+STARTUP: showeverything
#+INCLUDE: "~/.emacs.d/oer-reveal-org/config.org"
#+OPTIONS: reveal_width:1200 reveal_height:1000

#+TITLE: Demo for attribution with emacs-reveal
#+AUTHOR: Jens Lechtenbörger
#+DATE: March 2019

#+INCLUDE: "slides.org"
